// Set up your application entry point here...
import { render } from 'react-dom';
import React from 'react';
import { Router, Route, hashHistory } from 'react-router';

require('./favicon.ico'); // Tell webpack to load favicon.ico
// import './styles/styles.scss'; // Yep, that's right. You can import SASS/CSS files too! Webpack will run the associated loader and plug this into the page.

import App from './components/app';
import CreateQcm from './components/CreateQcmComponent';
import Login from './components/LoginComponent';
import PassQcm from './components/PassQcmComponent';
import Home from './components/HomeComponent';
import createAccount from './components/CreateAccountComponent';
import AskToPassQcm from './components/AskToPassQcmComponent';
// import history from './businessLogic/history';

//Needed for onTouchTap
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

let unlisten = hashHistory.listen(function(location) {
  if (location.pathname == '/' || !location.pathname) {
    // hashHistory.push({url:'/home/'});
    hashHistory.push('/home/');
    return;
  }
});

render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="login/" component={Login} />
      <Route path="createAccount/" component={createAccount} />
      <Route path="createQcm/*" component={CreateQcm}/>
      <Route path="askToPassQcm/:id" component={AskToPassQcm}/>
      <Route path="passExam/:id" component={PassQcm}/>
      <Route path="*" component={Home} />
    </Route>
  </Router>
  ), document.getElementById('app'));
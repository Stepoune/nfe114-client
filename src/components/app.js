//main libs
import React, { PropTypes } from 'react';
import { render } from 'react-dom';
import { hashHistory } from 'react-router';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

//App Logic
// import history from '../businessLogic/history';

//Style Import
import ActionHome from 'material-ui/svg-icons/action/home';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import RaisedButton from 'material-ui/RaisedButton';
// import './../styles/components/app.scss';
// import FontIcon from 'material-ui/FontIcon';

const style = {
  margin: 12
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this._testMethod = this._testMethod.bind(this);
    this.user = JSON.parse('{}');
    this.session = JSON.parse('{}');
    this.token = "lol";
    this.handleLocalStorageEvent = this.handleLocalStorageEvent.bind(this);
    this.handleUserUdpdate = this.handleUserUdpdate.bind(this);
    this.state = {
      user: this.user,
      session: this.session,
      test: 'test',
      token: this.token
    };
    this.goToHome = this.goToHome.bind(this);
    this.logout = this.logout.bind(this);
  }

  getChildContext() {
    //check si user connecté
    //et si connecté le met dans le contexte
    return {
      user: this.state.user,
      session: this.state.session,
      test: this.state.test,
      token: this.state.token
    };
  }

  handleLocalStorageEvent(storageEvent) {
    console.log('storageEvent', storageEvent);
    if (storageEvent.key === 'user' && storageEvent.storageArea === 'localStorage') {
      this.setState({
        user: JSON.parse(storageEvent.value)
      });
    }
  }

  handleUserUdpdate(event) {
    console.log('event', event.detail);
    if (event.detail) {
      this.setState({
        user: JSON.parse(event.detail)
      });
    } else {
      this.setState({
        user: null
      });
      hashHistory.push('/login/');
      return;
    }

  }


  componentWillMount() {
    console.log('BIG START');
    //at the start of the app, if no id -> go to state per default
    window.addEventListener('storage', this.handleLocalStorageEvent);
    window.addEventListener('userUpdated', this.handleUserUdpdate);
    console.log('JSON.parse', JSON.parse(localStorage.getItem('user')));
    this.setState({
      user: JSON.parse(localStorage.getItem('user'))
    });
    setTimeout(() => {
      console.log('this.state.user', this.state.user);
    }, 1000);
    console.log('prout');
  }

  _testMethod() {
    //history.replaceState(null, '/state/default');
  }

  goToHome() {
    hashHistory.push('/home/');
  }

  logout() {
    window.localStorage.clear();
    window.dispatchEvent(new CustomEvent('userUpdated', {
      'detail': null
    }));
  }

  render() {
    var user = '';
    if (this.state && this.state.user && this.state.user.email) {
      user = 'cher '+this.state.user.email;

    }
    var title = "Bienvenue sur l'appli QCM " +  user
    ;
    let childrens = ( <div>
      <AppBar
    title={title}
    iconElementLeft={<IconButton onClick={this.goToHome} ><ActionHome /></IconButton>}
    iconElementRight={
    <IconMenu
    iconButtonElement={
    <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{
      horizontal: 'right',
      vertical: 'top'
    }}
    anchorOrigin={{
      horizontal: 'right',
      vertical: 'top'
    }}
    >
            <MenuItem primaryText="Sign out" onClick={this.logout} />
          </IconMenu>
    }
    />
      {this.props.children}
    </div>);

    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        {childrens}
      </MuiThemeProvider>
      );
  }
}

// to have user in the context for the children of Component App
App.childContextTypes = {
  user: React.PropTypes.object,
  session: React.PropTypes.object,
  domain: React.PropTypes.string,
  test: React.PropTypes.string,
  token: React.PropTypes.string
};

App.propTypes = {
  children: PropTypes.element
};

export default App;
//main libs
import React, { PropTypes } from 'react';
import { hashHistory, Router } from 'react-router';

//Components
// import history from '../businessLogic/history';



//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';

import AllQcmsComponent from './AllQcmsComponent';


//import '../styles/components/state.scss';



class HomeComponent extends React.Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {

    };

    this._onClick = this._onClick.bind(this);
    this._onClickLogout = this._onClickLogout.bind(this);
  }

  componentWillMount() {
    //this.getList();
    console.log('componentWillMount');
    console.log(this.context);
    if (!this.context || !this.context.user || !this.context.user.token || this.context.user.type === undefined || this.context.user.type === null) {
      console.log('de home à login');
      hashHistory.push('/login/');
      return;
    }
  }





  _onClick(event) {
    console.log(this.context.user.type);

    if (this.context.user.type == 1) {
      //hashHistory.push({url:'/createQcm/'});
      hashHistory.push('/createQcm/');
      return;
    } else {

      // hashHistory.push({url:'/passQcm/'});
      hashHistory.push('/passQcm/');

      return;
    }

  }

  _onClickLogout(event) {
    window.localStorage.clear();
    window.dispatchEvent(new CustomEvent('userUpdated', {
      'detail': null
    }));
  }


  render() {

    var lesQcms = null;
    var renderDisplay = [];
    var typette = {
      who: null,
      what: null
    };
    if (this.context.user.type == 1) {
      typette.who = 'Teacher';
      typette.what = 'create';
    } else {
      typette.who = 'Student';
      typette.what = 'pass';
      lesQcms = <AllQcmsComponent key={'AllQcmsComponent'} />
    }

    renderDisplay.push(<div key={'div4'}>Vous êtes un {typette.who}</div>);
    if(typette.who === 'Teacher'){
          renderDisplay.push(<div key={'div5'} ><RaisedButton key={'button3'} label={typette.what} primary={true} onClick={this._onClick}/></div>);
    }


    return (
      <div className="MainComponent">
        <div><h1>Home</h1></div>
        {renderDisplay}
        <hr/>
        {lesQcms}
      </div>
      );
  }

}

HomeComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default HomeComponent;
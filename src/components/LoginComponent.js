//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';

//import '../styles/components/state.scss';


import axios from 'axios';


const serverUrl = 'http://127.0.0.1:8080/';

class LoginComponent extends React.Component {

  constructor(props, context) {
    console.log('constructor login');
    super(props);
    this.context = context;
    this.state = {
      email: null,
      password: null,
      token: this.context.token,
      user: this.context.user
    };

    this._onClick = this._onClick.bind(this);
    this.handleChangeLogin = this.handleChangeLogin.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  componentWillMount() {
    console.log('componentWillMount login');
  }

  componentDidMount() {
    console.log('componentDidMount Login 0');

    //check if auth in ls
    if (this.context && this.context.user && this.context.user.token) {
      hashHistory.push({
        url: '/home'
      });
      return;
    }
    console.log('componentDidMount Login');
  }


  handleChangePassword(event) {
    this.setState({
      password: event.target.value
    });
  }

  handleChangeLogin(event) {
    this.setState({
      email: event.target.value
    });
  }


  _onClick(event) {
    console.log('ee', this.state.login, this.state.password);
    axios.post(serverUrl + 'anon/authenticate', {
      email: this.state.email,
      password: this.state.password
    })
      .then((response) => {
        console.log('prout');
        console.log(response);
        if (response.data.success) {


          var user = response.data.user;
          user.token = response.data.token;

          this.setState({
            token: response.data.token,
            user: user
          });

          window.localStorage.setItem('user', null);
          window.localStorage.setItem('user', JSON.stringify(user));
          window.dispatchEvent(new CustomEvent('userUpdated', {
            'detail': JSON.stringify(user)
          }));

          console.log('this.state.token', this.state.token);
          console.log('this.context.token', this.context.token);

          hashHistory.push('/home/');
        }

      })
      .catch((response) => {
        console.log('prout');
        console.log(response);
      });
  }




  render() {


    return (
      <div className="MainComponent">
        <div>Login {this.context.token}</div>
        <TextField hintText="email" value={this.state.email}  onChange={this.handleChangeLogin} />
        <TextField hintText="password" value={this.state.password}  onChange={this.handleChangePassword} />
        <div><RaisedButton label="logue toi" primary={true} onClick={this._onClick}/></div>
        <a href ="#/createAccount/">Inscris toi</a>
        
      </div>
      );
  }

}

LoginComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default LoginComponent;
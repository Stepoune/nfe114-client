//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';

import CreateResponse from './CreateResponseComponent';
//import '../styles/components/state.scss';
import FontAwesome from 'react-fontawesome';


class CreateQuestionComponent extends React . Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      item: this.props.item
    };

    this.handleLabelQuestion = this.handleLabelQuestion.bind(this);
  }

  componentDidMount() {
    //this.getList();
  }


  handleLabelQuestion(event) {
    this.props.handleLabelQuestion(event.target.value, this.props.index);
  }


  render() {
    var responses = [];

    this.state.item.responses.map((item, index, array) => {
      var idResp = this.props.id + index + 'reponse';
      responses.push( <CreateResponse id={idResp} key={idResp} index={index} questionid={this.props.index} item={item} handleLabelQuestionUpdate={this.props.handleLabelQuestionUpdate} handleToggleUpdate={this.props.handleToggleUpdate}  onClickRemoveResponse={this.props.onClickRemoveResponse}/>);

    });

    return (
      <div className="MainComponent">
        <div><TextField id={this.props.id + 'label'} key={this.props.id + 'label'} hintText="Label Question"  value={this.state.item.label}  onChange={this.props.handleLabelQuestion.bind(this, this.props.index)} /></div>
        {responses}
        <div><RaisedButton label="add a response" primary={true} onClick={this.props.onClickAddResponse.bind(this, this.props.index)} /></div>
        <div><hr/></div>
        </div>
      );
  }

}

CreateQuestionComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default CreateQuestionComponent;

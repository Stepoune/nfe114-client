//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import {List, ListItem} from 'material-ui/List';

//import '../styles/components/state.scss';


import axios from 'axios';

const stylePaper = {
  width: 500,
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
};


const serverUrl = 'http://127.0.0.1:8080/';

class AllQcmsComponent extends React . Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      qcms: []
    };

  }

  componentWillMount() {
    //this.getList();
    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': this.context.user.token
    };
    axios({
      method: 'post',
      url: serverUrl + 'api/all/qcm',
      headers: header
    })
      .then((response) => {
        console.log('prout');
        console.log(response);
        this.setState({
          qcms: response.data
        });
      })
      .catch((response) => {
        console.log('prout error');
        console.log(JSON.stringify(response));
      });
  }


  render() {


    var renderDisplay = [];
    
    this.state.qcms.map(function(item, index, array) {
      var link = "#/askToPassQcm/" + item._id;
      renderDisplay.push(<a href={link} id={index}><ListItem primaryText={index + ' : ' + item.name}  /></a>);
      //renderDisplay.push(<div key={index}><a href={link} id={index}>{index + ' : ' + item.name}</a></div>);
    });
    


    return (
      <div className="MainComponent">
        <div><h2>Les qcms</h2></div>
        <Paper style={stylePaper} zDepth={1} >
          <List>
          {renderDisplay}
          </List>
        </Paper>
      </div>
      );
  }

}

AllQcmsComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default AllQcmsComponent;

//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import Slider from 'material-ui/Slider';
import Toggle from 'material-ui/Toggle';

import FontAwesome from 'react-fontawesome';

//import '../styles/components/state.scss';


import axios from 'axios';


const serverUrl = 'http://127.0.0.1:8080/';

class CreateResponseComponent extends React . Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      questionid: this.props.questionid,
      item: this.props.item
    };

    console.log('this.state.item', this.state.item);
    console.log('this.state.props', this.props.item);

  }

  componentDidMount() {
    //this.getList();
  }






  render() {

    const styles = {
      block: {
        maxWidth: 250
      },
      toggle: {
        marginBottom: 16
      }
    };


    var display = [];
    display.push(<div key={this.props.id + 'div1'}><TextField key={this.props.id + 'tf'} hintText="Response Question" value={this.state.item.label} onChange={this.props.handleLabelQuestionUpdate.bind(this, this.props.questionid, this.props.index)}/></div>);
    display.push(<Toggle key={this.props.id + 'tg'}
    label="Correct"
    style={styles.toggle}
    toggled={this.state.item.correct}
    onToggle={this.props.handleToggleUpdate.bind(this, this.props.questionid, this.props.index, !this.state.item.correct)}
    />);


    return (
      <div className="MainComponent">
      {display}
      </div>
      );
  }

}

CreateResponseComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default CreateResponseComponent;

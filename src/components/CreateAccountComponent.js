//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
//import '../styles/components/state.scss';


import axios from 'axios';


const serverUrl = 'http://127.0.0.1:8080/';

class createAccountComponent extends React . Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      email: null,
      password: null,
      name: null,
      type: 0,
      token: this.context.token,
      user: this.context.user
    };

    this._onClick = this._onClick.bind(this);
    this.handleChangeLogin = this.handleChangeLogin.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.onCheck = this.onCheck.bind(this);
  }

  componentDidMount() {
    //check if auth in ls
  }


  handleChangePassword(event) {
    this.setState({
      password: event.target.value
    });
  }

  handleChangeLogin(event) {
    this.setState({
      email: event.target.value
    });
  }

  handleChangeName(event) {
    this.setState({
      name: event.target.value
    });
  }

  onCheck(event, value) {
    console.log('onCheckStudent', event);
    this.setState({
      type: value
    });
  }

  _onClick(event) {
    console.log('ee', this.state.login, this.state.password);
    axios.post(serverUrl + 'anon/create/user', {
      email: this.state.email,
      password: this.state.password,
      name: this.state.name,
      type: parseInt(this.state.type)
    })
      .then((response) => {
        console.log('prout');
        console.log(response);
        if (response.data.success) {
          this.setState({
            token: response.data.token
          });

          var user = response.data.user;
          user.token = response.data.token;
          this.setState({
            token: response.data.token,
            user: user
          });
          window.localStorage.setItem('user', null);
          window.localStorage.setItem('user', JSON.stringify(user));
          window.dispatchEvent(new CustomEvent('userUpdated', {
            'detail': JSON.stringify(user)
          }));

          console.log('this.state.token', this.state.token);
          console.log('this.context.token', this.context.token);

          hashHistory.push({url:'/home'});
        }
      })
      .catch((response) => {
        console.log('prout');
        console.log(response);
      });
  }




  render() {

    const styles = {
      block: {
        maxWidth: 250
      },
      radioButton: {
        marginBottom: 16
      }
    };


    return (
      <div className="MainComponent">
      <div>Create your account</div>
      <TextField hintText="email" value={this.state.email}  onChange={this.handleChangeLogin} />
      <TextField hintText="name" value={this.state.name}  onChange={this.handleChangeName} />
      <hr/>
      <TextField hintText="password" value={this.state.password}  onChange={this.handleChangePassword} />
      <RadioButtonGroup onChange={this.onCheck} name="shipSpeed" defaultSelected="0">
    <RadioButton
      value="0"
      label="Student"
      style={styles.radioButton}
      />
    <RadioButton
      value="1"
      label="Teacher"
      style={styles.radioButton}
      />
    </RadioButtonGroup>
      <div><RaisedButton label="inscris toi" primary={true} onClick={this._onClick}/></div>
    </div>
      );
  }

}

createAccountComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default createAccountComponent;

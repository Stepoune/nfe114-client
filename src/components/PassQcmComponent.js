//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

//import '../styles/components/state.scss';

import Question from './QuestionComponent';


import axios from 'axios';

var timeoutID;
const serverUrl = 'http://127.0.0.1:8080/';

class PassQcmComponent extends React.Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      qcm: null,
      indexCurrentQuestion: null,
      time: null,
      note: null,
      open: false
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.onUpdateResponse = this.onUpdateResponse.bind(this);
    this.validation = this.validation.bind(this);
    this.handleClose = this.handleClose.bind(this);

  }

  componentWillMount() {
    //this.getList();

    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': this.context.user.token
    };

    axios({
      method: 'post',
      url: serverUrl + 'api/get/full/qcm',
      headers: header,
      data: {
        id: this.props.params.id
      }
    })
      .then((response) => {
        console.log('prout');
        console.log(response.data);
        this.setState({
          qcm: response.data,
          indexCurrentQuestion: 0
        });
      })
      .catch((response) => {
        console.log('prout error');
        console.log(JSON.stringify(response));
      });


  }

  componentDidMount() {

    var time = 1000 * 60 * 1;

    timeoutID = window.setInterval(() => {

      time = time - 1000;
      if (time < 1) {
        clearInterval(timeoutID);
        this.validation();
        return;
      }
      this.setState({
        time: time
      });
    }, 1000);
  }

  handleClose(){
    hashHistory.push('/home/');
  }


  onUpdateResponse(idQuestion, idResponse, trueValue, target, value) {
    console.log('pour cette question ', idQuestion, 'le gars a indiqué que ', idResponse, 'sont ' + trueValue);
    let qcm = this.state.qcm;
    qcm.questions.map((item, index, array) => {
      if (item._id === idQuestion) {
        item.responses.map((iteme, indexe, arraye) => {
          if (iteme._id === idResponse) {
            qcm.questions[index].responses[indexe].choosen = trueValue;
            console.log('value', trueValue);
          }
        });
      }
    });

    this.setState({
      qcm: qcm
    });
  }


  next() {
    console.log('next 2');
    if (this.state.indexCurrentQuestion < this.state.qcm.questions.length - 1) {

      console.log('next 3');
      var current = this.state.indexCurrentQuestion;
      this.setState({
        indexCurrentQuestion: current + 1
      });
    }
  }

  previous() {
    if (this.state.indexCurrentQuestion > 0) {
      var current = this.state.indexCurrentQuestion;

      this.setState({
        indexCurrentQuestion: current - 1
      });
    }
  }

  componentWillUnmount(){
    console.log('SHIIIIT');
    clearInterval(timeoutID);
  }

  validation() {
    clearInterval(timeoutID);
    console.log('validation', this.state.qcm);
    let objectToSend = {};
    objectToSend.token = this.context.user.token;
    objectToSend.resume = {};
    objectToSend.resume._id = this.state.qcm._id;
    objectToSend.resume.responsesOfUser = {};

    this.state.qcm.questions.map((item, index, array) => {
      objectToSend.resume.responsesOfUser[item._id] = [];
      // let aResponseOfUser = {};
      // aResponseOfUser[item._id] = [];
      // aResponseOfUser.question = item._id;
      // aResponseOfUser.responsesSelectedForTheQuestion = [];
      item.responses.map((iteme, indexe, arraye) => {
        if (iteme.choosen) {
          //aResponseOfUser[item._id].push(iteme._id);
          //aResponseOfUser.responsesSelectedForTheQuestion.push(iteme._id);
          objectToSend.resume.responsesOfUser[item._id].push(iteme._id);
        }
      });
    // objectToSend.resume.responsesOfUser[item._id].push(aResponseOfUser);
    });


    console.log('objectToSend', objectToSend);
    axios.post(serverUrl + 'api/check/resume', objectToSend)
      .then((response) => {
        console.log('response.data.noteTotal', response.data.noteTotal);

        this.setState({
          note: response.data.noteTotal
        });
        console.log('prout');
        window.setTimeout(() => {
          this.setState({
            open: true
          });
        }, 10);


      })
      .catch((response) => {
        console.log('prout error');
        console.log(response);
      });
  }


  render() {


    var qcmStringified = null;
    var aQuestion = null;
    var numberOfQuestion = null;
    var qcmName = null;
    var time = null;
    var note = null;
    var open;
    console.log('rerender 1', this.state.qcm);
    if (this.state.qcm && this.state.qcm.questions) {
      console.log('rerender 2');
      aQuestion = this.state.qcm.questions[this.state.indexCurrentQuestion];
      console.log('aQuestion', aQuestion, this.state.indexCurrentQuestion);
      numberOfQuestion = this.state.qcm.questions.length;
      qcmName = this.state.qcm.name;
      qcmStringified = JSON.stringify(this.state.qcm.questions);

    }

    if (this.state && this.state.time) {
      time = this.state.time / 1000;
    }

    // if (this.state & this.state.note) {
    //   note = this.state.note;
    //   console.log('note');
    // }

    if (this.state) {
      open = this.state.open;
    }

      const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div className="MainComponent">
       <Dialog
      title="Fin de l'examen"
      modal={false}
      actions={actions}
      open={open}
      >
      Le Qcm a été validé, vous avez {this.state.note}/10;
      </Dialog>
      <br/>
        <div>Question  {this.state.indexCurrentQuestion + 1 + '/' + numberOfQuestion}  </div>
        <p>Il reste <time>{time}</time> secondes.</p>
          <h3>{qcmName}</h3>
            <Question next={this.next} previous={this.previous} question={aQuestion} onUpdateResponse={this.onUpdateResponse} />
        <RaisedButton label="Valider" primary={true} onClick={this.validation.bind(this)}/>

      </div>
      );
  }

}

PassQcmComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default PassQcmComponent;
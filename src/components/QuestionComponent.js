//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';

//import '../styles/components/state.scss';



import axios from 'axios';


const serverUrl = 'http://127.0.0.1:8080/';

const styles = {
  block: {
    maxWidth: 250,
  },
  toggle: {
    marginBottom: 16,
  },
};


class QuestionComponent extends React.Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      question: null
    };

    this.nextQuestion = this.nextQuestion.bind(this);
    this.previousQuestion = this.previousQuestion.bind(this);

  }

  componentWillMount() {
    this.setState({
      question: this.props.question
    });
  }

  previousQuestion() {
    this.props.previous();

  }


  nextQuestion() {
    console.log('next 1');
    this.props.next();
  }


  render() {


    var questionStringified = JSON.stringify(this.props.question);
    var responses = [];
    var label = null;
    let id;
    if (this.props.question && this.props.question.responses) {

      this.props.question.responses.map((item, index, array) => {

        responses.push(
          <div >
            {item.label}
            <Toggle
              label="Response"
              style={styles.toggle}
              toggled={item.choosen}
              onToggle={this.props.onUpdateResponse.bind(this, this.props.question._id, item._id, !item.choosen)}
            />
          </div>);
      });
      label = this.props.question.label;
      id = this.props.question._id;

    }
    return (
      <div className="MainComponent">
        <h3>{label}</h3>
        {responses}
        <RaisedButton label="Previous" primary={true} onClick={this.previousQuestion}/>
        <RaisedButton label="next" primary={true} onClick={this.nextQuestion}/>
        <br/>
      </div>
      );
  }

}

QuestionComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default QuestionComponent;
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

import axios from 'axios';
const serverUrl = 'http://127.0.0.1:8080/';



class AskToPassQcmComponent extends React.Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      qcm: null
    }
  }


  componentWillMount() {
    //this.getList();

    var header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': this.context.user.token
    };

    axios({
      method: 'post',
      url: serverUrl + 'api/get/full/qcm',
      headers: header,
      data: {
        id: this.props.params.id
      }
    })
      .then((response) => {
        console.log('prout');
        console.log(response.data);
        this.setState({
          qcm: response.data
        });
      })
      .catch((response) => {
        console.log('prout error');
        console.log(JSON.stringify(response));
      });
  }


  render(){
        var numberOfQuestion = null;
    var qcmName = null;
    var link =null;
    if (this.state.qcm && this.state.qcm.questions) {
      numberOfQuestion = this.state.qcm.questions.length;
      qcmName = this.state.qcm.name;
      link = "#/passExam/" + this.state.qcm ._id;
    }

    return(
      <div className="MainComponent">
        Résumé du Qcm à passer {qcmName}<br/>
        Nombre de questions : {numberOfQuestion}<br/>
        Durée : 5min<br/>
        <a href={link} >passer {qcmName}</a>

      </div>
    );

  }


}

AskToPassQcmComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default AskToPassQcmComponent;
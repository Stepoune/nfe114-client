//main libs
import React, { PropTypes } from 'react';
import { hashHistory } from 'react-router';

//Components


//App Logic
// import history from '../businessLogic/history';

//Style Import
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import Slider from 'material-ui/Slider';
import Toggle from 'material-ui/Toggle';

import CreateQuestion from './CreateQuestionComponent';
import CreateResponse from './CreateResponseComponent';

//import '../styles/components/state.scss';
import FontAwesome from 'react-fontawesome';


import axios from 'axios';


const serverUrl = 'http://127.0.0.1:8080/';

class CreateQcmComponent extends React . Component {

  constructor(props, context) {
    super(props);
    this.context = context;
    this.state = {
      label: null,
      theme: null,
      description: null,
      difficulty: 1,
      questions: []
    };



    this._onClick = this._onClick.bind(this);
    this.onClickAddQuestion = this.onClickAddQuestion.bind(this);
    this.handleLabelQuestion = this.handleLabelQuestion.bind(this);
    this.onClickSaveQcm = this.onClickSaveQcm.bind(this);
    this.handleSliderDifficulty = this.handleSliderDifficulty.bind(this);
    this.handleChangeTheme = this.handleChangeTheme.bind(this);
    this.handleChangeLabel = this.handleChangeLabel.bind(this);

    this.onClickAddResponse = this.onClickAddResponse.bind(this);
    this.handleToggleUpdate = this.handleToggleUpdate.bind(this);
    this.handleLabelQuestionUpdate = this.handleLabelQuestionUpdate.bind(this);

    this.onClickRemoveQuestion = this.onClickRemoveQuestion.bind(this);
    this.onClickRemoveResponse = this.onClickRemoveResponse.bind(this);
  }

  componentDidMount() {
    //this.getList();
  }





  _onClick(event) {}

  handleChangeLabel(event) {
    this.setState({
      label: event.target.value
    });
  }

  handleChangeDescription(event) {
    this.setState({
      description: event.target.value
    });
  }

  handleChangeTheme(event) {
    this.setState({
      theme: event.target.value
    });
  }

  handleSliderDifficulty(event, value) {
    this.setState({
      difficulty: value
    });
  }

  onClickSaveQcm(event) {
    var objectToSend = {
      token: this.context.user.token,
      creator: this.context.user.id,
      theme: this.state.theme,
      name: this.state.label,
      description: this.state.description || this.state.label,
      difficulty: this.state.difficulty,
      questions: this.state.questions
    };

    var header ={
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': this.context.user.token
      };
    console.log(header);


    // axios.post(serverUrl + 'api/create/full/qcm', objectToSend)
    axios({
      method: 'post',
      url: serverUrl + 'api/create/full/qcm',
      data: objectToSend,
      headers: header
    })
      .then((response) => {
        console.log('prout');
        console.log(response);
        alert("success");
        hashHistory.push('/Home/');
      })
      .catch((response) => {
        console.log('prout error');
        console.log(JSON.stringify(response));
        alert(JSON.stringify(response));
        hashHistory.push('/Home/');
      });

  }

  onClickAddQuestion(event) {
    var questions = this.state.questions;
    questions.push({
      label: 'new question',
      responses: [{
        label: 'first response',
        correct: true
      }, {
        label: 'second response',
        correct: false
      }]
    });
    this.setState({
      questions: questions
    });
  }

  // handleLabelQuestion(newValue, index) {
  //  console.log('check', newValue, index);
  handleLabelQuestion(index, event) {
    var newValue = event.target.value;
    var questionsette = this.state.questions;
    questionsette[index].label = newValue;
    this.setState({
      questions: questionsette
    });
    console.log('state', this.state.questions);
    return;
  }

  onClickAddResponse(index, event) {
    var questions = this.state.questions;
    questions[index].responses.push({
      label: 'new response',
      correct: true
    });

    this.setState({
      questions: questions
    });
    return;
  }

  handleToggleUpdate(indexQ, indexR, newValue, event) {
    var questions = this.state.questions;

    questions[indexQ].responses[indexR].correct = newValue;

    this.setState({
      questions: questions
    });

    return;
  }

  handleLabelQuestionUpdate(indexQ, indexR, event) {
    var questions = this.state.questions;

    questions[indexQ].responses[indexR].label = event.target.value;

    this.setState({
      questions: questions
    });

    return;
  }

  onClickRemoveQuestion(indexQ, event) {
    var questions = this.state.questions;
    questions.splice(indexQ, 1);

    this.setState({
      questions: questions
    });

    return;
  }

  onClickRemoveResponse(indexQ, indexR, event) {
    var questions = this.state.questions;
    questions[indexQ].responses.splice(indexQ, 1);

    this.setState({
      questions: questions
    });

    return;
  }

  render() {

    const styles = {
      block: {
        maxWidth: 250
      },
      toggle: {
        marginBottom: 16
      }
    };

    var questions = [];

    this.state.questions.map((item, index, array) => {
      console.log('prout', item);
      var idTF = index + 'question';
      var copyItem = JSON.parse(JSON.stringify(item));
      questions.push(<CreateQuestion id={idTF} key={idTF} index={index} item={item} handleLabelQuestion={this.handleLabelQuestion} onClickAddResponse={this.onClickAddResponse} handleLabelQuestionUpdate={this.handleLabelQuestionUpdate} handleToggleUpdate={this.handleToggleUpdate} onClickRemoveResponse={this.onClickRemoveResponse} onClickRemoveQuestion={this.onClickRemoveQuestion}/>);
    });

    return (
      <div className="MainComponent">
          <div>Create qcm</div>
            <TextField hintText="label Qcm" value={this.state.label}  onChange={this.handleChangeLabel} />
            <TextField hintText="theme Qcm" value={this.state.theme}  onChange={this.handleChangeTheme} />
            <div>
           difficulty :  {this.state.difficulty}
            <Slider
      min={0}
      max={5}
      step={1}
      defaultValue={1}
      value={this.state.difficulty}
      onChange={this.handleSliderDifficulty}
      /></div>
            <div>Les questions : </div>
            <div>{questions}</div>
            <div><hr/></div>
            <div><RaisedButton label="add a question" primary={true} onClick={this.onClickAddQuestion} /></div>
            <div><hr/></div>
            <div><RaisedButton label="Save Qcm" primary={true} onClick={this.onClickSaveQcm} /></div>

        </div>
      );
  }

}

CreateQcmComponent.contextTypes = {
  token: React.PropTypes.string,
  user: React.PropTypes.object
};

export default CreateQcmComponent;
